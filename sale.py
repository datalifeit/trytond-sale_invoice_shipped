# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['SaleLine']


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def get_move(self, shipment_type):
        moves = [move for move in self.moves if move.state != 'cancelled']
        if self.sale.invoice_method == 'shipment' and moves:
            return
        return super(SaleLine, self).get_move(shipment_type)

    def get_move_done(self, name):
        done = True
        if self.type != 'line' or not self.product:
            return True
        if self.product.type == 'service':
            return True
        skip_ids = set(x.id for x in self.moves_ignored)
        skip_ids.update(x.id for x in self.moves_recreated)

        for move in self.moves:
            if move.state != 'done' \
                    and move.id not in skip_ids:
                done = False
                break

        return done
